@interface GPCPerson
-(id)displayPhoneNumber;
@end
@protocol GPCPersonItem
-(GPCPerson*)person;
@end
@interface GBMConversation
-(NSArray*)participants;
@end
@interface GBAConversationDetailsViewController
@property(nonatomic,assign) GBMConversation* conversation;
-(void)navigateToContactForPerson:(GPCPerson*)person;
@end
@interface GPCTBRow <GPCPersonItem>
@property(nonatomic,assign) void (^selectionHandler)(GPCTBRow*);
@end
@interface GPCTBSection
@property(nonatomic,assign) NSArray* rows;
@end
@interface GBAConversationViewController
-(void)handleActionBarSettingsTappedEvent:(id)event;
@end
@interface GBMHangoutIntent : NSObject
-(id)initWithDialedNumber:(NSString*)number regionCode:(NSString*)region;
@end
@interface GBMUserClient
-(NSString*)regionCode;
@end
@interface GBAPhoneCallNavigationContext
+(void)navigateToPhoneCallWithUserClient:(GBMUserClient*)client intent:(GBMHangoutIntent*)intent;
@end

%hook GBAConversationDetailsViewController
-(GPCTBSection*)participantsSectionWithAddButton:(BOOL)addButton includeSelf:(BOOL)includeSelf {
  GPCTBSection* section=%orig;
  for (GPCTBRow* row in section.rows){
    GPCPerson* person=row.person;
    if(person && person.displayPhoneNumber){
      row.selectionHandler=^(GPCTBRow* row){
       [self navigateToContactForPerson:person];};
    }
  }
  return section;
}
-(GPCTBSection*)personHeaderSection {
  GPCTBSection* section=%orig;
  NSArray* rows=section.rows;
  if(rows.count){
    GPCPerson* person=[(id<GPCPersonItem>)
     [self.conversation.participants objectAtIndex:0] person];
    if(person.displayPhoneNumber){
      GPCTBRow* row=[rows objectAtIndex:0];
      row.selectionHandler=^(GPCTBRow* row){
       [self navigateToContactForPerson:person];};
    }
  }
  return section;
}
%end
%hook GBAConversationViewController
-(void)didPressTitle {
  [self handleActionBarSettingsTappedEvent:nil];
}
%end
%hook GBMAppLaunchUrlHandler
-(id)appLaunchOptionsForInternalUrl:(NSURL*)URL isSecure:(BOOL)secure userClient:(GBMUserClient*)client {
  if([URL.host isEqualToString:@"call"]){
    GBMHangoutIntent* intent=[[%c(GBMHangoutIntent) alloc]
     initWithDialedNumber:URL.query regionCode:client.regionCode];
    [%c(GBAPhoneCallNavigationContext) navigateToPhoneCallWithUserClient:client intent:intent];
    [intent release];
    return nil;
  }
  return %orig;
}
%end
